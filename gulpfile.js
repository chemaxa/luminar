'use strict';
let gulp        = require('gulp');
let browserSync = require('browser-sync').create();
let sass        = require('gulp-sass');
let autoprefixer = require('gulp-autoprefixer');

let sassPaths=[ './node_modules/sass-mediaqueries','./node_modules/hamburgers/_sass/hamburgers','./sass/blocks'];
// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: {
            baseDir: "./",
            index: "about.html"
        }
    });

    gulp.watch("./sass/**/*.sass", ['sass']);
    gulp.watch("*.html").on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src("./sass/*.sass")
        .pipe(sass({
            includePaths: sassPaths
        }))
        .pipe(autoprefixer({ browsers: ['last 4 versions','ie 10'] })) 
        .pipe(gulp.dest("./css"))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);